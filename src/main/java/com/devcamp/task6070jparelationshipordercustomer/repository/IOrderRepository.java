package com.devcamp.task6070jparelationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070jparelationshipordercustomer.model.Order;

public interface IOrderRepository extends JpaRepository<Order, Long>{
    Order findById(long Id);
}
